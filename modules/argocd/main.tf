locals {
  argocd_values = {
    environment                     = terraform.workspace
    argocd_version                  = var.argocd_version
    argocd_server_secretkey         = random_password.argocd-server-secretkey.result
    argocd_admin_password_mtime     = null_resource.argocd-admin-password-bcrypt.triggers["time"]
    argocd_admin_password_bcrypted  = null_resource.argocd-admin-password-bcrypt.triggers["pw"]
    deploy_custom_css               = var.deploy_custom_css
    serviceMonitor                  = var.serviceMonitor
    argocd_css_nav_bar              = var.argocd_css_nav_bar
    git_base_url                    = var.git_base_url
    cert_manager_email              = var.cert_manager_email
    argocd_domain                   = var.argocd_domain
    ip_restriction                  = var.ip_restriction
  }
  slack_api_url     = var.slack_api_url
  ntfy_url          = var.ntfy_url
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_namespace" "kubernetes-argocd-namespace" {
  metadata {
    name = "argocd"
  }
}

resource "random_password" "argocd-admin-password" {
  length  = 64
  special = false
}

resource "random_password" "argocd-server-secretkey" {
  length = 64
  special = false
}

resource "random_password" "elastic-credential" {
  length = 64
  special = false
}

resource "kubernetes_secret" "argocd-admin-password" {
  metadata {
    name = "argocd-admin-pass-login"
    namespace = "argocd"
  }

  data = {
    username = "admin"
    password = null_resource.argocd-admin-password-bcrypt.triggers["orig"]
  }
}

resource "null_resource" "argocd-admin-password-bcrypt" {
  triggers = {
    orig = random_password.argocd-admin-password.result
    pw = bcrypt(random_password.argocd-admin-password.result)
    time = timestamp()
  }

  lifecycle {
    ignore_changes = [triggers["pw"], triggers["time"]]
  }
}

resource "helm_release" "argocd" {
  chart      = "argo-cd"
  name       = "argocd"
  namespace  = kubernetes_namespace.kubernetes-argocd-namespace.metadata[0].name
  repository = "https://argoproj.github.io/argo-helm"
  version    = var.argocd_helm_version

  values = [
    templatefile("${path.module}/resources/values-tmpl.yaml", local.argocd_values)
  ]

  timeout = 120
}

resource "kubectl_manifest" "apps-platform-project" {
  depends_on = [helm_release.argocd]
  yaml_body = file("${path.module}/resources/appproject.yaml")
}

resource "kubectl_manifest" "apps-devops-apps" {
  depends_on = [helm_release.argocd]
  yaml_body = templatefile("${path.module}/resources/apps-platform-apps-tmpl.yaml", {
    environment         = local.argocd_values.environment
    slack_api_url       = local.slack_api_url
    argocd_domain       = local.argocd_values.argocd_domain
    ntfy_url            = local.ntfy_url
    cert_manager_email  = local.argocd_values.cert_manager_email
    elastic_credential  = random_password.elastic-credential.result
    ip_restriction      = local.argocd_values.ip_restriction
  })
}
